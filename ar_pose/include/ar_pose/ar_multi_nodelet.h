#ifndef AR_POSE_AR_MULTI_NODELET_H
#define AR_POSE_AR_MULTI_NODELET_H

#include <nodelet/nodelet.h>

#include "ar_pose/ar_multi_common.h"

namespace ar_pose
{
  class ARMultiPublisherNodelet : public nodelet::Nodelet
  {
  public:
    ARMultiPublisherNodelet (void);
    ~ARMultiPublisherNodelet (void);
    virtual void onInit(void);

  private:

    ARCommon ar_com_;
    ros::NodeHandle n_;
  }; // end class ARMultiPublisher
} // end namespace ar_pose

#endif
