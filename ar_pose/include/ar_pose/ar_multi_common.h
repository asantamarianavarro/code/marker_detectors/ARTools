#ifndef AR_POSE_AR_MULTI_COMMON_H
#define AR_POSE_AR_MULTI_COMMON_H


#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include <artoolkit/AR/gsub.h>
#include <artoolkit/AR/video.h>
#include <artoolkit/AR/param.h>
#include <artoolkit/AR/ar.h>
#include <artoolkit/AR/arMulti.h>

#include <ros/ros.h>
#include <ros/package.h>
#include <ros/console.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/transform_broadcaster.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CameraInfo.h>
#include <visualization_msgs/Marker.h>
#include <resource_retriever/retriever.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#if ROS_VERSION_MINIMUM(1, 9, 0)
  // new cv_bridge API in Groovy
  #include <cv_bridge/cv_bridge.h> 
  #include <sensor_msgs/image_encodings.h> 
#else
  // Fuerte support for cv_bridge will be deprecated
  #if defined(__GNUC__)
    #warning "Support for the old cv_bridge API (Fuerte) is derecated and will be removed when Hydro is released."
  #endif
  #include <cv_bridge/CvBridge.h>
#endif

#include <ar_tools_msgs/ARMarkers.h>
#include <ar_tools_msgs/ARMarker.h>
#include <ar_tools_msgs/ContourArray.h>
#include <ar_pose/object.h>

#include <boost/thread/thread.hpp>


// typedef std::map<int, std::vector<>> MapType; 

const double AR_TO_ROS = 0.001;

typedef  std::map<int,std::vector<double> > sub_buff;
typedef std::map<std::string, sub_buff> meta_buff;

namespace ar_pose
{

  class Buffer
  {
    public:
      
      meta_buff data;
      int frame_buff_size; 

      Buffer(void);
      ~Buffer(void);

      void insert(const std::string& id, const int& frame, const std::vector<double>& vec_1D);
      void mean(const std::string& id, std::vector<double>& mean);
      void pose_mean(const std::string& id, std::vector<double>& new_pos);
      void print();
    private:

  };

  class ARCommon
  {
    public:

      ARCommon(void);
      ~ARCommon(void);

      void getROSParams(ros::NodeHandle& private_nh);
      void getROSObj(ros::NodeHandle& nh);

    private:

      boost::thread ImgThread_;
      bool RunImgThread_;
      void processImage(void);

      bool new_image_arrival_;
      sensor_msgs::Image::ConstPtr curr_img_;

      boost::shared_ptr<image_transport::ImageTransport> it_;

      ros::Subscriber sub_;
      image_transport::Subscriber cam_sub_;
      image_transport::Publisher imgContPub_;
      ros::Publisher arMarkerPub_;
      ros::Publisher rvizMarkerPub_;
      ros::Publisher contPub_;
      bool publishers_ok_;
      bool publish_img_;

      sensor_msgs::CameraInfo cam_info_;
      std::string name_space_;
      bool calibration_;
      bool publishTf_;
      bool publishVisualMarkers_;    
      std::string camera_frame_;
      std::vector<std::vector<float> > cont_model_points_;
      Buffer tag_buff_;
      Buffer img_buff_;
      
      ar_object::ObjectData_T * object_;
      int objectnum_;

      std::string cameraImageTopic_;
      std::string cameraInfoTopic_;
      bool getCamInfo_;
      char pattern_filename_[FILENAME_MAX];
      int threshold_;

      ARParam cam_param_;         // Camera Calibration Parameters
      CvSize sz_;
      ros::Time time_last_tracking_reset_; // Timer to reset the tracker.

      tf::TransformBroadcaster broadcaster_;
      visualization_msgs::Marker rvizMarker_;
      std::vector<std::vector<float> > cont_img_points_;
      ar_tools_msgs::ARMarkers arPoseMarkers_;

#if ROS_VERSION_MINIMUM(1, 9, 0)
    //cv_bridge::CvImagePtr capture_;
    cv_bridge::CvImageConstPtr capture_;
#else
    IplImage *capture_;
    sensor_msgs::CvBridge bridge_;
#endif

      void getTransformationCallback (const sensor_msgs::ImageConstPtr&  image_msg);
      void camInfoCallback (const sensor_msgs::CameraInfoConstPtr& cam_info);
      void arInit (void);
      std::vector<std::vector<float> > getContourImgPts(const sensor_msgs::ImageConstPtr& image_msg, const ar_object::ObjectData_T * object, const int i, const ARMarkerInfo *marker_info, const int k, Buffer& img_buff);
      void drawContours(cv::Mat img, std::vector<float> imgPts, std::string id);
      bool isSquare(std::vector<std::vector<float> >& cont_img_points);
      std::vector<float> read_modelxyz(FILE *model_file);
      float get2DRelAng(const std::vector<float>& vec1, const std::vector<float>& vec2);
      std::vector<float> vecSubstract(const std::vector<float>& vec1, const std::vector<float>& vec2);

      bool got_brightness_;
      void getBrightness(const cv::Mat& frame, double& brightness);

  }; // end namespace armulti_common

} // end namespace ar_pose

#endif
