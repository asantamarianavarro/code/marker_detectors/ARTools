/*
 *  Multi Marker Pose Estimation using ARToolkit
 *  Copyright (C) 2013, I Heart Engineering
 *  Copyright (C) 2010, CCNY Robotics Lab
 *  William Morris <bill@iheartengineering.com>
 *  Ivan Dryanovski <ivan.dryanovski@gmail.com>
 *  Gautier Dumonteil <gautier.dumonteil@gmail.com>
 *  http://www.iheartengineering.com
 *  http://robotics.ccny.cuny.edu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ar_pose/ar_multi.h"
#include "ar_pose/object.h"

int main (int argc, char **argv)
{
  ros::init (argc, argv, "ar_multi");
  ros::NodeHandle n;
  ar_pose::ARMultiPublisher ar_multi (n);
  ros::spin ();
  return 0;
}

namespace ar_pose
{
  ARMultiPublisher::ARMultiPublisher (ros::NodeHandle & n):n_ (n)
  {
    ros::NodeHandle n_param ("~");
    ar_com_.getROSParams(n_param);
    ar_com_.getROSObj(n_param);
  }

  ARMultiPublisher::~ARMultiPublisher (void)
  {
  }
} // end namespace ar_pose


