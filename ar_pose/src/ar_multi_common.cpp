
#include "ar_pose/ar_multi_common.h"

namespace ar_pose
{

  // Buffer class
  Buffer::Buffer(void)
  {}

  Buffer::~Buffer(void)
  {}

  void Buffer::insert(const std::string& id, const int& frame, const std::vector<double>& vec_1D)
  {
    sub_buff curr_data;
    sub_buff::iterator frame_it;
    std::map<std::string,sub_buff>::iterator tag_it = data.find(id);

    if (tag_it == data.end())
    {
      frame_it = curr_data.begin();
      curr_data.insert (frame_it, std::pair<int,std::vector<double> >(frame,vec_1D));

      tag_it = data.begin();
      data.insert (tag_it, std::pair<std::string,sub_buff>(id,curr_data));
    }
    else
    {
      if (tag_it->second.size() == frame_buff_size)
        tag_it->second.erase(tag_it->second.begin());

      frame_it = tag_it->second.begin();

      if (frame_it->first<frame-frame_buff_size || frame_it->first>frame)
        tag_it->second.clear();      

      curr_data = tag_it->second;
      sub_buff::iterator frame_it = curr_data.begin();
      curr_data.insert (frame_it, std::pair<int,std::vector<double> >(frame,vec_1D));
      tag_it->second = curr_data;

      frame_it = tag_it->second.begin();
    }
  }

  void Buffer::print()
  {
    std::cout << "Buffer data:\n";
    std::map<std::string,sub_buff>::iterator tag_it = data.begin();
    for (tag_it=data.begin(); tag_it!=data.end(); ++tag_it)
    {
      std::cout << tag_it->first;
      std::map<int,std::vector<double> >::iterator frame_it = tag_it->second.begin();
      for (frame_it=tag_it->second.begin(); frame_it!=tag_it->second.end(); ++frame_it)
      {
        std::cout << " => f: " << frame_it->first << "[ ";
        for (int ii = 0; ii < frame_it->second.size(); ++ii)
          std::cout << frame_it->second[ii] << ",";
        std::cout << "]\n";
      }
    }
  }

  void Buffer::mean(const std::string& id, std::vector<double>& mean)
  {
    std::map<std::string,sub_buff>::iterator tag_it = data.find(id);
    if (tag_it == data.end())
      std::cout << "Mean not computed. ID does not exist\n";
    else
    {
      // Mean
      std::map<int,std::vector<double> >::iterator frame_it = tag_it->second.begin();

      int num_frames = frame_it->second.size();
      
      std::vector<double> sum;
      sum.resize(num_frames);

      for (frame_it=tag_it->second.begin(); frame_it!=tag_it->second.end(); ++frame_it)
        for (int ii = 0; ii < num_frames; ++ii)
          sum[ii] += frame_it->second[ii];

      // position
      frame_it = tag_it->second.begin();
      for (int ii = 0; ii < frame_it->second.size(); ++ii)
        mean[ii] = sum[ii] / tag_it->second.size();

      // Debug ONLY 
      // std::cout << "id: " << id << " new pose: \n";
      // for (int ii = 0; ii < mean.size(); ++ii)
      //   std::cout << mean[ii] << ", ";
      // std::cout << std::endl;  
    }
  }

  void Buffer::pose_mean(const std::string& id, std::vector<double>& new_pos)
  {
    std::map<std::string,sub_buff>::iterator tag_it = data.find(id);
    if (tag_it == data.end())
      std::cout << "Mean not computed. ID does not exist\n";
    else
    {
      // Position and quaternion mean
      std::map<int,std::vector<double> >::iterator frame_it = tag_it->second.begin();

      int num_frames = frame_it->second.size();
      
      std::vector<double> sum;
      sum.resize(num_frames);

      for (frame_it=tag_it->second.begin(); frame_it!=tag_it->second.end(); ++frame_it)
        for (int ii = 0; ii < num_frames; ++ii)
          sum[ii] += frame_it->second[ii];

      // position
      for (int ii = 0; ii < 3; ++ii)
        new_pos[ii] = sum[ii] / tag_it->second.size();

      // quaternion
      frame_it = tag_it->second.begin();
      double quatmod = sqrt(pow(frame_it->second[3],2)+pow(frame_it->second[4],2)+pow(frame_it->second[5],2)+pow(frame_it->second[6],2));       
      for (int ii = 0; ii < 4; ++ii)
        for (int jj = 0; jj < tag_it->second.size(); ++jj)
          new_pos[3+ii] = sum[3+ii]/quatmod;

      // Debug ONLY 
      // std::cout << "id: " << id << " new pose: \n";
      // for (int ii = 0; ii < new_pos.size(); ++ii)
      //   std::cout << new_pos[ii] << ", ";
      // std::cout << std::endl;
    }
  }


  void ARCommon::getBrightness(const cv::Mat& frame, double& brightness)
  {
    cv::Mat temp, color[3], lum;
    temp = frame;

    cv::split(temp, color);

    color[0] = color[0] * 0.299;
    color[1] = color[1] * 0.587;
    color[2] = color[2] * 0.114;

    lum = color[0] + color [1] + color[2];

    cv::Scalar summ = cv::sum(lum);

    brightness = summ[0]/((cv::pow(2,8)-1)*frame.rows * frame.cols) * 2; //-- percentage conversion factor
  }



  void ARCommon::processImage(void)
  {
    while(RunImgThread_)
    {
      if (new_image_arrival_ && publishers_ok_)
      {
        ARUint8 *dataPtr;
        ARMarkerInfo *marker_info;
        int marker_num;
        int i, k, j;
    
        /* Get the image from ROSTOPIC
         * NOTE: the dataPtr format is BGR because the ARToolKit library was
         * build with V4L, dataPtr format change according to the 
         * ARToolKit configure option (see config.h).*/
    #if ROS_VERSION_MINIMUM(1, 9, 0)
        try
        {
          capture_ = cv_bridge::toCvCopy (curr_img_, sensor_msgs::image_encodings::BGR8);
          //capture_ = cv_bridge::toCvShare (curr_img_, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
          ROS_ERROR("cv_bridge exception: %s", e.what()); 
        }
        dataPtr = (ARUint8 *) ((IplImage) capture_->image).imageData;
    #else
        try
        {
          capture_ = bridge_.imgMsgToCv (curr_img_, "bgr8");
        }
        catch (sensor_msgs::CvBridgeException & e)
        {
          ROS_ERROR("cv_bridge exception: %s", e.what()); 
        }
        dataPtr = (ARUint8 *) capture_->imageData;
    #endif
    
        // Get orientative brightness in the first frame 
        if (!got_brightness_)
        {
          double brightness; 		
          getBrightness(capture_->image, brightness);
          // std::cout << "Orientative image brightness: " << brightness << std::endl;
          if (brightness < 0.5)
	          ROS_WARN("[ar_pose SUGGESTION]: Increase camera shutter time. Current brightness: %f", brightness);
          got_brightness_ = true;
        }


        // detect the markers in the video frame
        if (arDetectMarker (dataPtr, threshold_, &marker_info, &marker_num) < 0)
        {
          argCleanup ();
          ROS_BREAK ();
        }
    
        // Array of contours  
        std::vector<ar_tools_msgs::Contour> all_conts;
    
        // //Debug ONLY
        // std::cout << "**********" << std::endl;
        // std::cout << "IDS in all frames: [";
    
    
        arPoseMarkers_.markers.clear ();
        // check for known patterns
        for (i = 0; i < objectnum_; i++)
        {
          k = -1;
          for (j = 0; j < marker_num; j++)
          {
            if (object_[i].id == marker_info[j].id)
            {
              if (k == -1)
                k = j;
              else                  // make sure you have the best pattern (highest confidence factor)
              if (marker_info[k].cf < marker_info[j].cf)
                k = j;
            }
          }
          if (k == -1)
          {
            object_[i].visible = 0;
            continue;
          }
    
          // calculate the transform for each marker
          if (object_[i].visible == 0 )
          {
            arGetTransMat (&marker_info[k], object_[i].marker_center, object_[i].marker_width, object_[i].trans);
            time_last_tracking_reset_ = ros::Time::now();
          }
          else
          {
            arGetTransMatCont (&marker_info[k], object_[i].trans,
                               object_[i].marker_center, object_[i].marker_width, object_[i].trans);
          }
          if ((ros::Time::now().toSec()-time_last_tracking_reset_.toSec()) > 2.0)
            object_[i].visible = 0;
          else
            object_[i].visible = 1;
    
          double arQuat[4], arPos[3];
    
          //arUtilMatInv (object_[i].trans, cam_trans);
          arUtilMat2QuatPos (object_[i].trans, arQuat, arPos);
     
          // **** convert to ROS frame
    
          double quat[4], pos[3];
    
          pos[0] = arPos[0] * AR_TO_ROS;
          pos[1] = arPos[1] * AR_TO_ROS;
          pos[2] = arPos[2] * AR_TO_ROS;
    
          quat[0] = -arQuat[0];
          quat[1] = -arQuat[1];
          quat[2] = -arQuat[2];
          quat[3] = arQuat[3];
    
          // Check negative Z
          if (pos[2]<0)
          {
            pos[0] = -pos[0];
            pos[1] = -pos[1];
            pos[2] = -pos[2];
          }
    
          ROS_DEBUG (" Object num %i ------------------------------------------------", i);
          ROS_DEBUG (" QUAT: Pos x: %3.5f  y: %3.5f  z: %3.5f", pos[0], pos[1], pos[2]);
          ROS_DEBUG ("     Quat qx: %3.5f qy: %3.5f qz: %3.5f qw: %3.5f", quat[0], quat[1], quat[2], quat[3]);
    
          
          // Obtain an average of last out_buff_size_ poses _________________
          std::vector<double> new_pos;
          for (int ii = 0; ii < 3; ++ii)
            new_pos.push_back(pos[ii]);
          for (int ii = 0; ii < 4; ++ii)
            new_pos.push_back(quat[ii]);
    
          tag_buff_.insert(object_[i].name,curr_img_->header.seq,new_pos);
        
          tag_buff_.pose_mean(object_[i].name,new_pos);
    
          for (int ii = 0; ii < 3; ++ii)
            pos[ii] = new_pos[ii];
          for (int ii = 0; ii < 4; ++ii)
            quat[ii] = new_pos[3+ii];
    
          //________________________________________________________________
    
          if (marker_info->cf * 100 > 0)
          {
            // Get contour points in image
            cont_img_points_.clear();
            cont_img_points_ = getContourImgPts(curr_img_,object_,i,marker_info,k,img_buff_); 
    
            // // DEBUG ONLY 
            // std::cout << "___________________" << std::endl;
            // std::cout << " 2D image points:" << std::endl;
            // for (int uu = 0; uu < cont_img_points_.size(); ++uu)
            //   std::cout << cont_img_points_[uu][0] << ",  " << cont_img_points_[uu][1] << std::endl;
            // std::cout << " 3D model points:" << std::endl;
            // for (int ii = 0; ii < cont_model_points_.size(); ++ii)
            //   std::cout << cont_model_points_[ii][0] << ", " << cont_model_points_[ii][1] << ", " << cont_model_points_[ii][2] << std::endl;
            // std::cout << "___________________" << std::endl;
    
            // Check if obtained contours form a square        
            if(isSquare(cont_img_points_))
            {
              // Marker contours
              std::vector<float> imgPts; 
              std::vector<float> modelPts; 
        
              for (int uu = 0; uu < cont_img_points_.size(); ++uu)
              {
                imgPts.push_back(cont_img_points_[uu][0]);
                imgPts.push_back(cont_img_points_[uu][1]);
              }
              for (int ii = 0; ii < cont_model_points_.size(); ++ii)
              {
                modelPts.push_back(cont_model_points_[ii][0]);
                modelPts.push_back(cont_model_points_[ii][1]);
                modelPts.push_back(cont_model_points_[ii][2]);
              }
           
              //Image with contours and number
              if (publish_img_)  
                drawContours(capture_->image, imgPts, object_[i].name);
        
              // **** publish the marker
        
              ar_tools_msgs::ARMarker ar_pose_marker;
              ar_pose_marker.header.frame_id = curr_img_->header.frame_id;
              ar_pose_marker.header.stamp = curr_img_->header.stamp;
              ar_pose_marker.id = object_[i].id;
        
              ar_pose_marker.pose.pose.position.x = pos[0];
              ar_pose_marker.pose.pose.position.y = pos[1];
              ar_pose_marker.pose.pose.position.z = pos[2];
        
              ar_pose_marker.pose.pose.orientation.x = quat[0];
              ar_pose_marker.pose.pose.orientation.y = quat[1];
              ar_pose_marker.pose.pose.orientation.z = quat[2];
              ar_pose_marker.pose.pose.orientation.w = quat[3];
        
              ar_pose_marker.confidence = round(marker_info->cf * 100);
        
              arPoseMarkers_.markers.push_back (ar_pose_marker);
        
              //Contours and marker message
              ar_tools_msgs::Contour cont;
              cont.id = atoi(object_[i].name);
              cont.ImgPts = imgPts;
              cont.ModelPts = modelPts;
      
              cont.marker = ar_pose_marker;
              all_conts.push_back(cont);
      
              // **** publish transform between camera and marker
        
    #if ROS_VERSION_MINIMUM(1, 9, 0)
              tf::Quaternion rotation (quat[0], quat[1], quat[2], quat[3]);
              tf::Vector3 origin (pos[0], pos[1], pos[2]);
              tf::Transform t (rotation, origin);
    #else
    // DEPRECATED: Fuerte support ends when Hydro is released
              btQuaternion rotation (quat[0], quat[1], quat[2], quat[3]);
              btVector3 origin (pos[0], pos[1], pos[2]);
              btTransform t (rotation, origin);
    #endif
        
                if (publishTf_)
                {
                  std::stringstream obj_name;
                  obj_name << "/" << name_space_ << object_[i].name;
        
                  if (calibration_)
                  {
                    tf::StampedTransform MarkerTocam (t.inverse(), curr_img_->header.stamp, obj_name.str(), camera_frame_);
                    broadcaster_.sendTransform(MarkerTocam);
                  }
                  else
                  {
                    tf::StampedTransform camToMarker (t, curr_img_->header.stamp, camera_frame_, obj_name.str());
                    broadcaster_.sendTransform(camToMarker);
                  }
                }
        
              // **** publish visual marker
        
              if (publishVisualMarkers_)
              {
    #if ROS_VERSION_MINIMUM(1, 9, 0)
                tf::Vector3 markerOrigin (0, 0, 0.25 * object_[i].marker_width * AR_TO_ROS);
                tf::Transform m (tf::Quaternion::getIdentity (), markerOrigin);
                tf::Transform markerPose = t * m; // marker pose in the camera frame 
    #else
    // DEPRECATED: Fuerte support ends when Hydro is released
                btVector3 markerOrigin (0, 0, 0.25 * object_[i].marker_width * AR_TO_ROS);
                btTransform m (btQuaternion::getIdentity (), markerOrigin);
                btTransform markerPose = t * m; // marker pose in the camera frame
    #endif
        
                tf::poseTFToMsg (markerPose, rvizMarker_.pose);
        
                rvizMarker_.header.frame_id = curr_img_->header.frame_id;
                rvizMarker_.header.stamp = curr_img_->header.stamp;
                rvizMarker_.id = object_[i].id;
        
                rvizMarker_.scale.x = 1.0 * object_[i].marker_width * AR_TO_ROS;
                rvizMarker_.scale.y = 1.0 * object_[i].marker_width * AR_TO_ROS;
                rvizMarker_.scale.z = 0.5 * object_[i].marker_width * AR_TO_ROS;
                rvizMarker_.ns = "basic_shapes";
                rvizMarker_.type = visualization_msgs::Marker::CUBE;
                rvizMarker_.action = visualization_msgs::Marker::ADD;
                switch (i)
                {
                  case 0:
                    rvizMarker_.color.r = 0.0f;
                    rvizMarker_.color.g = 0.0f;
                    rvizMarker_.color.b = 1.0f;
                    rvizMarker_.color.a = 1.0;
                    break;
                  case 1:
                    rvizMarker_.color.r = 1.0f;
                    rvizMarker_.color.g = 0.0f;
                    rvizMarker_.color.b = 0.0f;
                    rvizMarker_.color.a = 1.0;
                    break;
                  default:
                    rvizMarker_.color.r = 0.0f;
                    rvizMarker_.color.g = 1.0f;
                    rvizMarker_.color.b = 0.0f;
                    rvizMarker_.color.a = 1.0;
                }
                rvizMarker_.lifetime = ros::Duration (1.0);
        
                rvizMarkerPub_.publish(rvizMarker_);
                ROS_DEBUG ("Published visual marker");
              }
            }
          }
        }
        if (publish_img_) 
          imgContPub_.publish(capture_->toImageMsg());
    
        // arPoseMarkers_.header = curr_img_->header;
    
        ar_tools_msgs::ContourArray cont_array;
        cont_array.header = curr_img_->header;
        cont_array.contours = all_conts;
        if (cont_array.contours.size() > 0)
        {
          contPub_.publish(cont_array);
        }
    
        arMarkerPub_.publish(arPoseMarkers_);
        ROS_DEBUG ("Published ar_multi markers");
    
        //Debug ONLY
        // tag_buff_.print();
        // img_buff_.print();

        new_image_arrival_ = false;

        ROS_DEBUG("Image processed <<<<<<<");
      }
    }
  }

  // ARMulti Common class
  // ARCommon::ARCommon(void)
  ARCommon::ARCommon(void): publishers_ok_(false), RunImgThread_(true), new_image_arrival_(false), ImgThread_(boost::bind(&ARCommon::processImage, this))
  {
    got_brightness_ = false;
  }

  ARCommon::~ARCommon(void)
  {
    RunImgThread_ = false;
    ImgThread_.join();
    arVideoCapStop ();
    arVideoClose ();   
  }

  void ARCommon::camInfoCallback (const sensor_msgs::CameraInfoConstPtr& cam_info)
  {
    if (!getCamInfo_)
    {
      cam_info_ = (*cam_info);

      cam_param_.xsize = cam_info_.width;
      cam_param_.ysize = cam_info_.height;

      cam_param_.mat[0][0] = cam_info_.P[0];
      cam_param_.mat[1][0] = cam_info_.P[4];
      cam_param_.mat[2][0] = cam_info_.P[8];
      cam_param_.mat[0][1] = cam_info_.P[1];
      cam_param_.mat[1][1] = cam_info_.P[5];
      cam_param_.mat[2][1] = cam_info_.P[9];
      cam_param_.mat[0][2] = cam_info_.P[2];
      cam_param_.mat[1][2] = cam_info_.P[6];
      cam_param_.mat[2][2] = cam_info_.P[10];
      cam_param_.mat[0][3] = cam_info_.P[3];
      cam_param_.mat[1][3] = cam_info_.P[7];
      cam_param_.mat[2][3] = cam_info_.P[11];

      cam_param_.dist_factor[0] = cam_info_.K[2];       // x0 = cX from openCV calibration
      cam_param_.dist_factor[1] = cam_info_.K[5];       // y0 = cY from openCV calibration
      if ( cam_info_.distortion_model == "plumb_bob" && cam_info_.D.size() == 5)
        cam_param_.dist_factor[2] = -100*cam_info_.D[0];// f = -100*k1 from CV. Note, we had to do mm^2 to m^2, hence 10^8->10^2
      else
        cam_param_.dist_factor[2] = 0;                  // We don't know the right value, so ignore it

      cam_param_.dist_factor[3] = 1.0;                  // scale factor, should probably be >1, but who cares...
      
      arInit ();

      // ROS_INFO ("Subscribing to image topic");
      // cam_sub_ = it_.subscribe (cameraImageTopic_, 1, &ARMultiPublisher::getTransformationCallback, this);
      getCamInfo_ = true;
    }
  }

  void ARCommon::arInit (void)
  {
    arInitCparam (&cam_param_);
    ROS_INFO ("*** Camera Parameter ***");
    arParamDisp (&cam_param_);

    // load in the object data - trained markers and associated bitmap files
    if ((object_ = ar_object::read_ObjData (pattern_filename_, &objectnum_)) == NULL)
      ROS_BREAK ();
    ROS_DEBUG ("Objectfile num = %d", objectnum_);

    sz_ = cvSize (cam_param_.xsize, cam_param_.ysize);
#if ROS_VERSION_MINIMUM(1, 9, 0)
// FIXME: Why is this not in the object
    cv_bridge::CvImagePtr capture_; 
#else
// DEPRECATED: Fuerte support ends when Hydro is released
    capture_ = cvCreateImage (sz_, IPL_DEPTH_8U, 4);
#endif
  }

  void ARCommon::getROSParams(ros::NodeHandle& private_nh)
  {
    std::string local_path;
    std::string package_path = ros::package::getPath (ROS_PACKAGE_NAME);
    std::string default_path = "data/object_4x4";
    std::string default_cont_model_path = package_path + "/data/cont_models/cont_004x004";
    XmlRpc::XmlRpcValue xml_marker_center;

    // **** get parameters
    if (!private_nh.getParam("namespace",name_space_))
      name_space_ = "markers/";
    ROS_INFO ("\tTF markers name_space: %s", name_space_.c_str());

    if (!private_nh.getParam ("calibration", calibration_))
      calibration_ = false;
    ROS_INFO ("\tCalibration Mode: %d", calibration_);

    if (!private_nh.getParam ("publish_tf", publishTf_))
      publishTf_ = true;
    ROS_INFO ("\tPublish transforms: %d", publishTf_);

    if (!private_nh.getParam("camera_frame",camera_frame_))
      camera_frame_ = "optical_frame";
    ROS_INFO ("\tCamera frame: %s", camera_frame_.c_str());

    if (!private_nh.getParam ("publish_visual_markers", publishVisualMarkers_))
      publishVisualMarkers_ = true;
    ROS_INFO ("\tPublish visual markers: %d", publishVisualMarkers_);

    if (!private_nh.getParam ("threshold", threshold_))
      threshold_ = 100;
    ROS_INFO ("\tThreshold: %d", threshold_);
  
    if (!private_nh.getParam("camera_info_topic",cameraInfoTopic_))
      cameraInfoTopic_ = "/camera/camera_info";
    ROS_INFO ("\tCamera Info topic: %s", cameraInfoTopic_.c_str());

    if (!private_nh.getParam("image_topic",cameraImageTopic_))
      cameraImageTopic_ = "/camera/image_raw";
    ROS_INFO ("\tImage topic: %s", cameraImageTopic_.c_str());

    //modifications to allow path list from outside the package
    private_nh.param ("marker_pattern_list", local_path, default_path);
    if (local_path.compare(0,5,"data/") == 0){
      //according to previous implementations, check if first 5 chars equal "data/"
      sprintf (pattern_filename_, "%s/%s", package_path.c_str (), local_path.c_str ());
    }
    else{
      //for new implementations, can pass a path outside the package_path
      sprintf (pattern_filename_, "%s", local_path.c_str ());
    }
    ROS_INFO ("\tMarker Pattern Filename: %s", pattern_filename_);
    
    // Model Contours file
    std::string cont_model_path;
    private_nh.param ("cont_model_path", cont_model_path, default_cont_model_path);

    // Buffer size (default 1)
    int out_buff_size; // output buffer size (average)
    if (!private_nh.getParam ("out_buff_size", out_buff_size))
      out_buff_size = 1;
    tag_buff_.frame_buff_size = out_buff_size;
    img_buff_.frame_buff_size = out_buff_size;
    ROS_INFO ("\tOutput buffer size: %d", out_buff_size);

    //Initialize model file and vars
    FILE *model_file = fopen(cont_model_path.c_str(), "r");

    int modelsize;
    if(fscanf(model_file, "%d", &modelsize)!=1)
      ROS_DEBUG("Could not read number of contour points in model file");

    for (int ii=0; ii < modelsize; ii++)
      cont_model_points_.push_back(read_modelxyz(model_file));

    // Check if image is needed
    if (!private_nh.getParam ("publish_image", publish_img_))
      publish_img_ = true;
  }

  void ARCommon::getROSObj(ros::NodeHandle& nh)
  {
    // Subscribers
    ROS_INFO ("Subscribing to info topic");
    sub_ = nh.subscribe (cameraInfoTopic_, 1, &ARCommon::camInfoCallback, this);
    getCamInfo_ = false;

    it_.reset(new image_transport::ImageTransport(nh));
    cam_sub_ = it_->subscribe(cameraImageTopic_, 1, &ARCommon::getTransformationCallback, this);

    // Publishers
    arMarkerPub_ = nh.advertise < ar_tools_msgs::ARMarkers > ("ar_pose_marker", 0);
    if(publishVisualMarkers_)
    {
      rvizMarkerPub_ = nh.advertise < visualization_msgs::Marker > ("visualization_marker", 0);
    }
    contPub_ = nh.advertise<ar_tools_msgs::ContourArray>("contours", 1);

    imgContPub_ = it_->advertise("camera/cont_image", 1);

    time_last_tracking_reset_ = ros::Time::now();   

    publishers_ok_ = true; 
  }

  void ARCommon::getTransformationCallback (const sensor_msgs::ImageConstPtr& image_msg)
  {
    if (!new_image_arrival_)
    {
      curr_img_ = image_msg;
      new_image_arrival_ = true;
    }
    ROS_DEBUG("New image received");
  }

  std::vector<float> ARCommon::read_modelxyz(FILE *model_file)
  {
    std::vector<float> xyz;
    float ftmp;
    for (int ii = 0; ii < 3; ++ii)
    {
      if(fscanf(model_file, "%f", &ftmp)!=1)
        ROS_DEBUG("[read_modelxyz]: Error reading contour file");
      xyz.push_back(ftmp);
    }
    return xyz;
  }

  std::vector<std::vector<float> > ARCommon::getContourImgPts(const sensor_msgs::ImageConstPtr& image_msg, const ar_object::ObjectData_T * object, const int i, const ARMarkerInfo *marker_info, const int k, Buffer& img_buff)
  {
    std::vector<std::vector<float> > cont_img_points;

    cont_img_points.clear();
    int d = marker_info[k].dir;

    double tl_x,tl_y,tr_x,tr_y,br_x,br_y,bl_x,bl_y;
    br_x = marker_info[k].vertex[(4-d)%4][0];
    br_y = marker_info[k].vertex[(4-d)%4][1];
    bl_x = marker_info[k].vertex[(5-d)%4][0]; 
    bl_y = marker_info[k].vertex[(5-d)%4][1];
    tl_x = marker_info[k].vertex[(6-d)%4][0];
    tl_y = marker_info[k].vertex[(6-d)%4][1];
    tr_x = marker_info[k].vertex[(7-d)%4][0];
    tr_y = marker_info[k].vertex[(7-d)%4][1];

    std::vector<double> img_p;
    img_p.push_back(br_x);
    img_p.push_back(br_y);
    img_p.push_back(bl_x);
    img_p.push_back(bl_y);
    img_p.push_back(tl_x);
    img_p.push_back(tl_y);
    img_p.push_back(tr_x);
    img_p.push_back(tr_y);
    img_buff.insert(object[i].name,curr_img_->header.seq,img_p);

    img_buff.mean(object[i].name,img_p);
    br_x = img_p[0]; 
    br_y = img_p[1];
    bl_x = img_p[2];
    bl_y = img_p[3];
    tl_x = img_p[4];
    tl_y = img_p[5];
    tr_x = img_p[6];
    tr_y = img_p[7];

    double div_y = (tl_y-bl_y)/4;
    double div_x = (tl_x-bl_x)/4;

    // 0: top left
    std::vector<float> point_uv;
    point_uv.push_back(tl_x);
    point_uv.push_back(tl_y);
    cont_img_points.push_back(point_uv);
    // 1
    point_uv.clear();
    point_uv.push_back(tl_x-div_x);
    point_uv.push_back(tl_y-div_y);
    cont_img_points.push_back(point_uv);
    // 2
    point_uv.clear();
    point_uv.push_back(tl_x-(2*div_x));
    point_uv.push_back(tl_y-(2*div_y));
    cont_img_points.push_back(point_uv);
    // 3
    point_uv.clear();
    point_uv.push_back(tl_x-(3*div_x));
    point_uv.push_back(tl_y-(3*div_y));
    cont_img_points.push_back(point_uv);
    // 4: bottom left
    point_uv.clear();
    point_uv.push_back(bl_x);
    point_uv.push_back(bl_y);
    cont_img_points.push_back(point_uv);
    // 5: bottom right
    point_uv.clear();
    point_uv.push_back(br_x);
    point_uv.push_back(br_y);
    cont_img_points.push_back(point_uv); 
    // 6
    point_uv.clear();
    point_uv.push_back(br_x+div_x);
    point_uv.push_back(br_y+div_y);
    cont_img_points.push_back(point_uv);         
    // 7
    point_uv.clear();
    point_uv.push_back(br_x+(2*div_x));
    point_uv.push_back(br_y+(2*div_y));
    cont_img_points.push_back(point_uv); 
     // 8
    point_uv.clear();
    point_uv.push_back(br_x+(3*div_x));
    point_uv.push_back(br_y+(3*div_y));
    cont_img_points.push_back(point_uv);         
    // 9: Top right
    point_uv.clear();
    point_uv.push_back(tr_x);
    point_uv.push_back(tr_y);
    cont_img_points.push_back(point_uv);        
 
    return cont_img_points;
  }

  void ARCommon::drawContours(cv::Mat img, std::vector<float> imgPts, std::string id)
  {
    ROS_DEBUG("Drawing contours in image");
    for (int ii = 0; ii < imgPts.size(); ii = ii+20)
    {
      std::vector<cv::Point> list;
      int npts = 10; 
      cv::Point p1,p3;     
      for (int pp = 0; pp < npts*2; pp = pp + 2)
      {
        int pidx = ii+pp;
        double x = imgPts[pidx];
        double y = imgPts[pidx+1];
        
        unsigned int xp = (unsigned int)round(x);
        unsigned int yp = (unsigned int)round(y);
        
        cv::Point p(xp,yp); 

        if (pp == 0)  
          cv::circle(img, p, 10, CV_RGB(255,0,0),-1);
        // else
          // cv::circle(img, p, 10, CV_RGB(255,0,128),-1);


        list.push_back(p);

        if (pp==0)
          p1=p;
        if (pp==10)
          p3=p;
      }
      const cv::Point *pts = (const cv::Point *)cv::Mat(list).data;

      cv::polylines(img, &pts,&npts, 1, true, CV_RGB(0,255,0), 3, CV_AA, 0);
      cv::Point middlep((unsigned int)((p1.x+p3.x)/2),(unsigned int)((p1.y+p3.y)/2));
      cv::putText(img, id, middlep, cv::FONT_HERSHEY_PLAIN, 4, CV_RGB(255,135,0),3);      
    }
  }

  bool ARCommon::isSquare(std::vector<std::vector<float> >& cont_img_points)
  {
    // In any affine transformation at least two sides of the square must be parallel
    bool is_square = false;

    // Get corner points
    std::vector<float> p1 = cont_img_points.at(0); // Top left
    std::vector<float> p2 = cont_img_points.at(4); // Bottom left
    std::vector<float> p3 = cont_img_points.at(5); // Bottom right
    std::vector<float> p4 = cont_img_points.at(9); // Top right

    // get vectors (square edges)
    std::vector<float>  vec1 = vecSubstract(p1,p2);
    std::vector<float>  vec2 = vecSubstract(p2,p3);
    std::vector<float>  vec3 = vecSubstract(p3,p4);
    std::vector<float>  vec4 = vecSubstract(p1,p4);  

    // Get relative angles between faced edges of the square
    float ang13 = get2DRelAng(vec3,vec1);
    float ang24 = get2DRelAng(vec2,vec4);

    std::vector<float>  vec13 = vecSubstract(vec1,vec3);
    std::vector<float>  vec24 = vecSubstract(vec2,vec4);

    // 0.1 rad diff
    if (std::abs(ang13)<0.1 && std::abs(ang24)<0.05)
      is_square = true;
    return is_square;
  }

  // Get relative angle between two 2D vectors
  float ARCommon::get2DRelAng(const std::vector<float>& vec1, const std::vector<float>& vec2)
  {
    float dotp = vec1.at(0)*vec2.at(0) + vec1.at(1)*vec2.at(1); // Dot product
    float det = vec1.at(0)*vec2.at(1) - vec1.at(1)*vec2.at(0); // Determinant
    float angle = std::atan2(det, dotp);
    return angle;
  }

  // Subtreaction between two vectors
  std::vector<float> ARCommon::vecSubstract(const std::vector<float>& vec1, const std::vector<float>& vec2)
  {
    std::vector<float> diff;
    diff.resize(vec1.size());
    for (int ii = 0; ii < vec1.size(); ++ii)
      diff.at(ii) = std::abs(vec1.at(ii)-vec2.at(ii));
    return diff;
  }
}  // end namespace ar_pose
