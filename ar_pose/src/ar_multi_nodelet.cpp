// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

#include "ar_pose/ar_multi_nodelet.h"
#include "ar_pose/object.h"

namespace ar_pose
{
  ARMultiPublisherNodelet::ARMultiPublisherNodelet (void)
  {
  }

  ARMultiPublisherNodelet::~ARMultiPublisherNodelet (void)
  {
  }

  void ARMultiPublisherNodelet::onInit(void) 
  {
    NODELET_INFO("Initializing nodelet...");

    ros::NodeHandle& private_nh = getPrivateNodeHandle();
    n_ = getNodeHandle();


    ar_com_.getROSParams(private_nh);
    ar_com_.getROSObj(n_);
  }  

  PLUGINLIB_DECLARE_CLASS(ar_pose, ARMultiPublisherNodelet, ar_pose::ARMultiPublisherNodelet, nodelet::Nodelet);
} // end namespace ar_pose
