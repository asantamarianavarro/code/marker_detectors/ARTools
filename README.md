# AR_TOOLS #

ROS metapackage to work with ARToolkit library.

## External Dependencies ###

* Glut

`sudo apt-get install libfreeglut3-dev` 

* Video4Linux

`sudo apt-get install ibv4l-dev`

## Main differences from original ar_tools package

* Catkinized
* Extracted ar_pose messages to avoid dependencies to ar_pose (thus, to artoolkit library installation)
* Publishes a topic with the external marker contours (detected 2D and defined 3D)
* Publishes a topic with the internal marker contours (detected 2D and defined 3D)

## Installation
* Install dependencies
* Download 

`git clone https://gitlab.com/asantamarianavarro/code/marker_detectors/ARTools.git`

* Compile

`catkin_make -DCATKIN_WHITELIST_PACKAGES="artoolkit" && catkin_make -DCATKIN_WHITELIST_PACKAGES=""`
